#!/bin/sh

/usr/bin/java -Xmx5g -cp /home/jmtorres/corenlp/stanford-corenlp-full-2018-02-27/stanford-corenlp-3.9.1.jar:/home/jmtorres/corenlp/stanford-corenlp-full-2018-02-27/stanford-corenlp-3.9.1-models.jar edu.stanford.nlp.pipeline.StanfordCoreNLPServer -preload tokenize,ssplit,pos,lemma,parse
