#!/usr/bin/python

import json
from collections import defaultdict
import sys
import os
import subprocess
import time
import nltk
import requests
import traceback
from nltk.tokenize import word_tokenize
from nltk.corpus import treebank
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet as wn
from nltk.parse.corenlp import CoreNLPParser, CoreNLPServer

os.environ['CLASSPATH'] = '/home/jmtorres/corenlp/stanford-corenlp-full-2018-02-27'

class DialogueItem():
  def __init__(self, items):
    self.kind = items[3]
    self.topic = items[4]
    self.topicid = items[0]
    self.infoid = items[1]
    self.text = items[5].decode('latin-1')
    self.disp_or_index = items[0]
    self.idn = items[0]
    self.faction = items[0]
    self.speaker_id = items[0]
    self.speaker_race = items[0]
    self.speaker_class = items[13]
    self.speaker_faction = items[0]
    self.speaker_rank = items[0]
    self.speaker_cell = items[0]
    self.pc_faction = items[0]
    self.pc_rank = items[0]
    self.speaker_sex = items[0]
    self.speaker_disp = items[0]
    self.fun_list = [FunCondition(items[18], items[19], items[20], items[21]), \
                     FunCondition(items[22], items[23], items[24], items[25]), \
                     FunCondition(items[26], items[27], items[28], items[29]), \
                     FunCondition(items[30], items[31], items[32], items[33]), \
                     FunCondition(items[34], items[35], items[36], items[37]), \
                     FunCondition(items[38], items[39], items[40], items[41])]
    self.result = items[42]
  def str(self):
    return 'Kind = {0} - Topic = "{1}"\n \
TopicID = {2} - InfoID = {3}\n \
Text = {4}\n \
Funs = {5}\n%s\n'.format(self.kind, self.topic, self.topicid, self.infoid, self.text, self.fun_list)
            
  __repr__ = str

class FunCondition():
  def __init__(self, name, param, cond, val):
    self.name = name
    self.param = param
    self.cond = cond
    self.val = val
  def str(self):
    return str(self.__dict__)
  __repr__ = str

class MWDialogueReader():
  def __init__(self, fname):
    self.fname = fname
    self.file = None
#   def __iter__(self):
#     # Return a new handle on the file
#     if self.file:
#       self.file.close()
#     self.file = open(self.fname, 'r')
#     return self
#   def next(self):
#     return None

def read_items():
  reader = MWDialogueReader('csmwinddial.txt')
  f = open(reader.fname, 'r')
  kinds = defaultdict(lambda: [0, set()])
  ditems = []
  for line in f:
    terms = line.split('\t')[:-1]
    for i in range(0,len(terms)):
      terms[i] = terms[i].strip('"')
    item = DialogueItem(terms)
    kind = terms[3]
    kinds[kind][0] += 1
    kinds[kind][1].add(len(terms))
    if item.infoid == '1921131430108332615':
      #terms[2] == '13039228981365825921':
      print(item)
    if item.kind == 'Topic':
      ditems.append(item)
  return (kinds, ditems)

def start_nlp_server(nlp_port):
  server = CoreNLPServer(port=nlp_port, java_options='-Xmx5g', corenlp_options=['-preload', 'pos'])
  parser = None
  try:
    server.start()
  except nltk.parse.corenlp.CoreNLPServerError:
    print('Ignoring first error ')
  started = False
  delay = 20
  while not started:
    try:
      parser = CoreNLPParser(url=''.join(['http://localhost:', str(nlp_port)]))
      print(parser.parse_text('The brwon fox jumped', properties={'annotators': 'pos'}).next())
      #print(parser.parse_text('The brwon fox jumped', properties={'annotators': 'pos'}).next())
      started = True
    except (requests.exceptions.ConnectionError, nltk.parse.corenlp.CoreNLPServerError) as err:
      print('Server not yet started, retrying')
      print(''.join(['==> ', str(err)]))
      print(str(subprocess.check_output('ps -ef | grep java', shell=True)))
      time.sleep(delay)
  return (server, parser)

def dump_loaded_texts(file_prefix):
  (kinds, ditems) = read_items()
  print(kinds)
  text_items = [it.text for it in ditems]
  f = open(''.join([file_prefix, '.text.json']), "w")
  json.dump(text_items, f, indent=2)
  f = open(''.join([file_prefix, '.full.json']), "w")
  json.dump([it.__dict__ for it in ditems], f, indent=2, default=str)

def syntactic_analysis(parag_list, prefix):
  nlp_port = 9000
  # (server, parser) = start_nlp_server(nlp_port)
  parser = CoreNLPParser(url=''.join(['http://localhost:', str(nlp_port)]))
  analyze_lines(parag_list, parser, prefix)
  # server.stop()

def load_text_dump(file_prefix):
  f = open(''.join([file_prefix, '.text.json']), 'r')
  text_items = json.load(f)
  return text_items

def load_full_dump(file_prefix):
  f = open(''.join([file_prefix, '.full.json']), 'r')
  ditems = json.load(f)
  return ditems

def analyze_lines(parag_list, parser, prefix):
  len_parag_list = len(parag_list)
  delay = 20
  i = 0
  stop = 300
  words_per_category = defaultdict(set)
  lemmatizer = WordNetLemmatizer()

  for parag in parag_list[0:stop]:
    i += 1
    print("Parsing line {0} of {1}".format(i, len_parag_list))
    # for (word, categ) in nltk.pos_tag(word_tokenize(ditems[3].text)):
    # postagged = nltk.pos_tag(word_tokenize(ditem.text))
    try:
      ok = False
      while not ok:
        try:
          # for sentence in parser.parse_text(parag, properties={'annotators': 'pos'}):
          for sentence in parser.parse_text(parag):
            postagged = sentence.pos()
            # print(postagged)
            for (word, categ) in postagged:
              if categ.startswith('V'):
                words_per_category[categ].add(lemmatizer.lemmatize(word.lower(), pos='v'))
              elif categ.startswith('N'):
                words_per_category[categ].add(lemmatizer.lemmatize(word.lower(), pos='n'))
          ok = True
        except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) as err:
          print ("Internal error, retrying...")
          time.sleep(delay)
    except:
      print("Error in fragment {} with contents:".format(i))
      print(parag)
      traceback.print_exc()

  dump_dialogue_info(parag_list, words_per_category, ''.join([prefix, '.dialogue.info']))
  dump_all_verbs(words_per_category, ''.join([prefix, '.verb.info']))

def main():
  prefix = 'mwind_dialogue'
  # dump_loaded_texts('mwind_dialogue')
  # text_items = load_text_dump(prefix)
  # print(text_items[300])
  # syntactic_analysis(text_items, prefix)
  semantic_analysis(prefix)

def semantic_analysis (prefix):
  verb_list = json.load(open(''.join([prefix, '.verb.info'])))
  # some_k = 400
  # a_verb = verb_list[some_k]
  # print(a_verb)
  # print(wn.synsets(a_verb))
  # for syn in wn.synsets(a_verb):
  #   next = syn
  #   print(syn, " -> ", list(reversed(syn.hypernym_paths())))
  #   print("  ", syn.definition())
  #   print("  ", syn.name())
  #   print("  hypernims: ", syn.hypernyms())
  #   print("  hyponims: ", syn.hyponyms())
  #   for lemma in syn.lemmas():
  #     print("  ", (" | ".join(lemma.frame_strings())))

  synset_tree = Tree()
  verb_limit = 50

  # for verb in verb_list:
  # for verb in verb_list[0:verb_limit]:
  for verb in verb_list:
    for syn in wn.synsets(verb):
      if (syn.name().split('.')[1] != 'v'): 
        break
      for path in syn.hypernym_paths():
        cursor = synset_tree
        for synnode in path:
          cursor = cursor.children[synnode.name()]
        cursor.val = verb

  synset_tree_json = synset_tree.to_dict()
  # print json.dumps(synset_tree.to_dict(), indent=2)
  synsets = open("synsets.json", 'w')
  json.dump(synset_tree_json, synsets, indent=2)
  synsets.close()

  for k1,v1 in synset_tree.children.items():
    print(k1)
    print(defn(k1))
    print("=" + str(v1.val))
    for k2,v2 in v1.children.items():
      print("  " + k2)
      print("  " + defn(k2))
      print("  =" + str(v2.val))

def dump_dialogue_info(ditems, words_per_category, dialogue_file_name):
  dialogue_info = {}
  dialogue_info['lines'] = len(ditems)
  dialogue_info['keys'] = words_per_category.keys()
  dialogue_info['words'] = words_per_category
  info_file = open(dialogue_file_name, "w")
  print('Dumping info')
  json.dump(dialogue_info, info_file, indent=2, default=list)

def dump_all_verbs(words_per_category, verb_file_name):
  lemmatizer = WordNetLemmatizer()
  all_verbs = set()
  for categ in words_per_category:
    if categ.startswith('V'):
      print ("Adding category: ", categ)
      for word in words_per_category[categ]:
        # { Part-of-speech constants
        # ADJ, ADJ_SAT, ADV, NOUN, VERB = 'a', 's', 'r', 'n', 'v'
        # }
        all_verbs.add(lemmatizer.lemmatize(word.lower(), pos='v'))
    else:
      print ("Excluding category: ", categ)
  all_verbs_list = sorted(all_verbs)
  verb_file = open(verb_file_name, "w")
  print("Dumping info")
  json.dump(all_verbs_list, verb_file, indent=2, default=list)

class Tree():
  def __init__(self):
    self.children = defaultdict(Tree)
    self.val = None
  def __repr__(self):
    return "{{val={0} => {1}}}".format(repr(self.val), repr(self.children))
  def to_dict(self):
    return ["=" + str(self.val), len(self.children), {k: [defn(k), v.to_dict()] for k, v in self.children.items()}]

def defn(sy):
  if sy and '.' in sy:
    return wn.synset(sy).definition()
  else:
    return "---"

if __name__== "__main__":
  main()
